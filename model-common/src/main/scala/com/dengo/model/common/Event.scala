package com.dengo.model.common

/**
  * @author Sem Babenko
  */
class Event(var id: String, var name: String)
  extends Serializable {}
